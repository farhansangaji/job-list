package main

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
)

func jsonReader(url string, target any) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}

	if resp.StatusCode == 500 {
		return errors.New("no data from JSON")
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body) // response body is []byte
	if err != nil {
		return err
	}

	if err := json.Unmarshal(body, &target); err != nil { // Parse []byte to the go struct pointer
		return err
	}

	return nil
}
