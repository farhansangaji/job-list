1. Import postman collection (```DANS Interview.postman_collection.json```) into your postman application
2. Make database and put it in ```.env``` file, environment variable format can be found in ```.env.example```
3. Run ```go mod tidy```
4. Run ```go run .```
5. You can login with seeder account username = admin and password = 123456