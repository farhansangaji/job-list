package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	"golang.org/x/crypto/bcrypt"
)

var db *sqlx.DB

const (
	identityKey = "id"
	userName    = "name"
)

func main() {
	err := godotenv.Load()
	gin.SetMode(os.Getenv("GIN_MODE"))
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	dbCredential := fmt.Sprintf("%s:%s@(%s:%s)/%s", os.Getenv("DBUSERNAME"), os.Getenv("DBPASSWORD"), os.Getenv("DBHOST"), os.Getenv("DBPORT"), os.Getenv("DBNAME"))
	db, err = sqlx.Connect("mysql", dbCredential)
	if err != nil {
		log.Fatalln(err)
	}

	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte("123456"), bcrypt.DefaultCost)
	db.Exec(migrateTable)
	db.Exec(fmt.Sprintf(seedTable, hashedPassword))

	r := gin.Default()
	r.Use(CORSMiddleware())

	authMiddleware, err := JWTSetter([]byte(os.Getenv("SECRET_KEY")))

	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}

	r.POST("/login", authMiddleware.LoginHandler)

	auth := r.Group("/auth")
	auth.GET("/refresh_token", authMiddleware.RefreshHandler)
	auth.Use(authMiddleware.MiddlewareFunc())
	{
		auth.GET("/", jobListHandler)
		auth.GET("/detail/:id", jobDetailHandler)
	}

	r.Run(fmt.Sprintf("%s:%s", os.Getenv("HOST"), os.Getenv("PORT")))
}
