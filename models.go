package main

type LoginBody struct {
	Username string `json:"username" db:"username" binding:"required"`
	Password string `json:"password" db:"password" binding:"required"`
}

type User struct {
	ID        int64  `json:"id" db:"id"`
	Name      string `json:"name" db:"name"`
	Username  string `json:"username" db:"username"`
	Password  string `json:"password" db:"password"`
	CreatedAt string `json:"created_at" db:"created_at"`
}

type JobListParam struct {
	Search   *string `form:"search"`
	Location *string `form:"location"`
	Fulltime *string `form:"full_time" binding:"omitempty,boolean"`
	Page     *string `form:"page" binding:"omitempty,numeric"`
}

type JobList struct {
	ID          *string `json:"id"`
	Type        *string `json:"type"`
	URL         *string `json:"url"`
	CreatedAt   *string `json:"created_at"`
	Company     *string `json:"company"`
	CompanyURL  *string `json:"company_url"`
	Location    *string `json:"location"`
	Title       *string `json:"title"`
	Description *string `json:"description"`
	HowToApply  *string `json:"how_to_apply"`
	CompanyLogo *string `json:"company_logo"`
}
