package main

import (
	"fmt"
	"net/http"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func loginHandler(c *gin.Context) (interface{}, error) {
	var user User
	var loginVals LoginBody

	if err := c.ShouldBind(&loginVals); err != nil {
		return nil, jwt.ErrMissingLoginValues
	}

	if err := db.Get(&user, "SELECT * FROM users WHERE username = ?", loginVals.Username); err != nil {
		return nil, jwt.ErrFailedAuthentication
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(loginVals.Password)); err != nil {
		return nil, jwt.ErrFailedAuthentication
	}

	return &user, nil
}

func jobListHandler(c *gin.Context) {
	var result []*JobList
	var param JobListParam
	url := "http://dev3.dansmultipro.co.id/api/recruitment/positions.json?"

	if err := c.ShouldBindQuery(&param); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}

	if param.Search != nil {
		url = fmt.Sprintf("%sdescription=%s&", url, *param.Search)
	}

	if param.Location != nil {
		url = fmt.Sprintf("%slocation=%s&", url, *param.Location)
	}

	if param.Fulltime != nil {
		url = fmt.Sprintf("%sfull_time=%s&", url, *param.Fulltime)
	}

	if param.Page != nil {
		url = fmt.Sprintf("%spage=%s&", url, *param.Page)
	}

	if err := jsonReader(url, &result); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, result)
}

func jobDetailHandler(c *gin.Context) {
	var result *JobList

	jobID := c.Param("id")
	url := fmt.Sprintf("http://dev3.dansmultipro.co.id/api/recruitment/positions/%s", jobID)

	if err := jsonReader(url, &result); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, result)
}
