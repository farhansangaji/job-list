package main

const (
	migrateTable = `
	CREATE TABLE IF NOT EXISTS users (
		id int NOT NULL AUTO_INCREMENT,
		name varchar(100) NOT NULL,
		username varchar(100) NOT NULL,
		password varchar(100) NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		PRIMARY KEY (id),
		UNIQUE KEY users_UN (username)
	  ) 
	ENGINE=InnoDB 
	AUTO_INCREMENT=1
	DEFAULT
	CHARSET=utf8mb4
	COLLATE=utf8mb4_0900_ai_ci;
	`

	seedTable = `
	INSERT INTO users (name,username,password)
	SELECT * FROM (SELECT 'admin 1', 'admin', '%s') AS tmp
	WHERE NOT EXISTS (
		SELECT name FROM users WHERE id = 1
	) LIMIT 1;
	`
)